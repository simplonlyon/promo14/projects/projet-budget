# Gestion de Budget
L'objectif de ce projet est de créer une application de gestion de budget avec un back en Node/Express et un front en React


## Core Features

L'application doit permettre de gérer son budget : Indiquer les différentes dépenses (et éventuellement entrées) d'argent avec le montant, la catégorie de dépense/entrée et un petit titre si on souhaite le préciser.

### Diagramme de Use Case

![Use Case Core features](./budget-use-case.png)


## Features Optionnelles
Dans les features optionnelles, on pourrait accéder à un graphique de nos dépenses par mois (sous forme de pie chart par exemple), ou bien gérer les catégories indépendamment des opérations (dans leur propre table), ou encore indiquer un budget à ne pas dépasser par mois avec un code couleur quand on s'approche de ce budget ou qu'on le dépasse

### Diagramme de Use Case
![Use Case Optional features](./budget-usecase-opt.png)

## Réalisation
Il va falloir créer une tranche complète de la base de donnée jusqu'à l'application. Nous aurons donc 2 applications, le serveur node.js et le front en React.

### Proposition d'organisation
* Faire un diagramme de classe représentant les entités qui persisteront (si vous ne faites pas les features optionnelles, il n'y aura qu'une entité à priori)
* Créer une script SQL avec création de la table et insertion de quelques données de tests
* Créer l'application node.js/express avec la structure habituelle
* Créer l'entité et le repository pour la table
* Créer le contrôleur/les routes pour la table en question
* Créer une application React et les différents components 

C'est une approche data first, si on souhaite, on peut également créer des components avec des fausses données 
au début et les remplacer à terme par les vrais appels serveur. On peut également faire une tranche complète pour chaque fonctionnalité de la bdd à react.
